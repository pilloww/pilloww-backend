import json
# noinspection PyUnresolvedReferences
import app.main
from app.model.base import Session
from app.model.entities import Hospital, Address, GeoCoordinates


def main():
    objects = json.load(open('hospitals.json', 'r'))["elements"]
    session = Session()

    for object in objects:
        if 'name' not in object['tags']:
            continue

        addr = Address(
            line1=f"{object['tags']['addr:street']} {object['tags']['addr:housenumber']}",
            line2=None,
            locality=object['tags']['addr:city'],
            postal_code=object['tags']['addr:postcode']
        ) if {'addr:street', 'addr:housenumber', 'addr:city', 'addr:postcode'}.issubset(object['tags'].keys()) else None

        if addr is not None:
            session.add(addr)
            session.commit()

        coord = GeoCoordinates(
            latitude=object['lat'],
            longitude=object['lon']
        ) if {'lat', 'lon'}.issubset(object.keys()) else None

        if 'center' in object.keys() and coord is None:
            center = object['center']
            coord = GeoCoordinates(
                latitude=center['lat'],
                longitude=center['lon']
            ) if {'lat', 'lon'}.issubset(center.keys()) else None

        if coord is not None:
            session.add(coord)
            session.commit()

        h = Hospital(
            label=object['tags']['name'],
            address=addr,
            coordinates=coord,
            osm_id=object['id'] if 'id' in object.keys() else None
        )

        print("adding " + h.label, addr, coord)

        session.add(h)

    session.commit()
    session.close()


if __name__ == '__main__':
    main()
