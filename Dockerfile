FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

ENV DATABASE_URL=sqlite:///db.sqlite
ENV PYTHONUNBUFFERED=1 


COPY ./requirements.txt .
RUN pip install -r requirements.txt
COPY ./app ./app
COPY importer.py .
COPY hospitals.json .

EXPOSE 80

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
