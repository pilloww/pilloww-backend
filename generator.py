import requests
import random


def generate_resources():
    base = 'http://localhost:8000'
    hospitals = requests.get(f'{base}/hospitals').json()['hospitals']

    for hospital in hospitals:
        # h_id = hospital['id']
        # for resource_type, label in [('normalCare', 'Normales Bett'), ('intermediateCare', 'Zwischenpflegebett'), ('intensiveCare', 'Intensivstationsbett')]:
        #     resource_params = {
        #         'hospital_id': h_id,
        #         'type': resource_type,
        #         'label': label
        #     }
        #     resource = requests.post(
        #         f'{base}/resource/add',
        #         json=resource_params,
        #         headers={'Authorization': 'Ca3jair2ao5aingaigha'}
        #     ).json()
        #     resource_id = resource['id']
        #     available_amount = random.randint(50, 500)
        #     utilized_amount = random.randint(0, available_amount)
        #     requests.post(
        #         f'{base}/resource/availability/{resource_id}',
        #         json={'count': available_amount},
        #         headers={'Authorization': 'Ca3jair2ao5aingaigha'}
        #     )
        #     requests.post(
        #         f'{base}/resource/utilization/{resource_id}',
        #         json={'count': utilized_amount},
        #         headers={'Authorization': 'Ca3jair2ao5aingaigha'}
        #     )
        #     print(f"added resources to hospital {hospital['name']}")
        for resource in hospital['resources']:
            if resource['utilized_amount'] == 0:
                continue
            avail = resource['available_amount']
            util = resource['utilized_amount']

            new_util = random.randint(util, (util + avail * 2) // 3)
            requests.post(
                f'{base}/resource/utilization/{resource["id"]}',
                json={'count': new_util},
                headers={'Authorization': 'Ca3jair2ao5aingaigha'}
            )



def main():
    generate_resources()


if __name__ == '__main__':
    main()
