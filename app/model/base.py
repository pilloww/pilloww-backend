import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


def is_production():
    return os.environ.get('ENVIRONMENT', None) != 'DEVELOPMENT'


def db_url():
    if is_production():
        print("DATABASE ENVIRONMENT: Production")
        url = os.environ.get('DATABASE_URL')
        if url is None:
            raise ValueError('Missing environment variable DATABASE_URL')
        return url
    else:
        print("DATABASE ENVIRONMENT: Development")
        return 'sqlite:///db.sqlite'


engine = create_engine(db_url())
Base = declarative_base()

Session = sessionmaker(bind=engine, autocommit=False)
