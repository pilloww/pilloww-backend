import uuid
from datetime import datetime
from typing import List, Optional

from sqlalchemy import Column, Integer, String, Text, Float, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from app.model.base import Base


class Address(Base):
    __tablename__ = 'Addresses'
    address_id = Column(Integer, primary_key=True, autoincrement=True)

    address_line1 = Column(Text)
    address_line2 = Column(Text, nullable=True)
    postal_code = Column(String(10))
    locality = Column(Text)

    def __init__(self, line1: str, line2: Optional[str], postal_code: str, locality: str):
        self.address_line1 = line1
        self.address_line2 = line2
        self.postal_code = postal_code
        self.locality = locality

    def __repr__(self):
        return f"{self.address_line1} ({self.address_line2}), {self.postal_code} {self.locality}"


class GeoCoordinates(Base):
    __tablename__ = 'GeoCoordinates'

    coordinates_id = Column(Integer, primary_key=True, autoincrement=True)
    latitude = Column(Float)
    longitude = Column(Float)

    def __init__(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return f"GeoCoordinates(lat={self.latitude}, lon={self.longitude})"


class Hospital(Base):
    __tablename__ = 'Hospitals'

    hospital_id = Column(String(64), primary_key=True)
    osm_id = Column(String(64), nullable=True)
    label = Column(Text)
    address_id = Column(Integer, ForeignKey(Address.address_id), nullable=True)
    coordinates_id = Column(Integer, ForeignKey(GeoCoordinates.coordinates_id), nullable=True)

    address = relationship('Address')
    coordinates = relationship('GeoCoordinates')
    resources: List['Resource'] = relationship('Resource', back_populates='hospital')

    def __init__(self, label: str, address: Address, coordinates: GeoCoordinates, osm_id: Optional[str] = None):
        self.hospital_id = str(uuid.uuid4())
        self.label = label
        self.address_id = address.address_id if address is not None else None
        self.coordinates_id = coordinates.coordinates_id if coordinates is not None else None
        self.osm_id = osm_id


class Resource(Base):
    __tablename__ = 'Resources'

    resource_id = Column(String(64), primary_key=True)
    resource_type = Column(String(256))
    label = Column(Text)
    description = Column(Text, nullable=True)

    hospital_id = Column(String(64), ForeignKey(Hospital.hospital_id), nullable=True)
    hospital = relationship('Hospital', back_populates='resources')

    availabilityChanges: List['ResourceAvailabilityTransaction'] = relationship('ResourceAvailabilityTransaction', back_populates='resource', cascade='delete')
    utilizationChanges: List['ResourceAvailabilityTransaction'] = relationship('ResourceUtilizationTransaction', back_populates='resource', cascade='delete')
    alerts: List['ResourceAlert'] = relationship('ResourceAlert', back_populates='resource', cascade='delete')

    def __init__(self, label: str, type: str, hospital: Hospital, description: Optional[str] = None):
        self.resource_id = str(uuid.uuid4())
        self.label = label
        self.resource_type: str = type
        self.description = description
        self.hospital_id = hospital.hospital_id


class ResourceTransactionMixin:
    transaction_id = Column(String(64), primary_key=True)
    timestamp = Column(DateTime)
    count = Column(Integer)


class ResourceAvailabilityTransaction(Base, ResourceTransactionMixin):
    __tablename__ = 'ResourceAvailabilityTransactions'

    resource_id = Column(String(64), ForeignKey(Resource.resource_id))
    resource = relationship('Resource', back_populates='availabilityChanges')

    def __init__(self, resource: Resource, count: int):
        self.transaction_id = str(uuid.uuid4())
        self.count = count
        self.resource_id = resource.resource_id
        self.timestamp = datetime.utcnow()


class ResourceUtilizationTransaction(Base, ResourceTransactionMixin):
    __tablename__ = 'ResourceUtilizationTransactions'

    resource_id = Column(String(64), ForeignKey(Resource.resource_id))
    resource = relationship('Resource', back_populates='utilizationChanges')

    def __init__(self, resource: Resource, count: int):
        self.transaction_id = str(uuid.uuid4())
        self.count = count
        self.resource_id = resource.resource_id
        self.timestamp = datetime.utcnow()


class ResourceAlert(Base):
    __tablename__ = 'ResourceAlert'

    alert_id = Column(String(64), primary_key=True)
    timestamp = Column(DateTime)
    title = Column(Text)
    message = Column(Text)

    resource_id = Column(String(64), ForeignKey(Resource.resource_id))
    resource = relationship('Resource', back_populates='alerts')

    def __init__(self, resource: Resource, title: str, message: str):
        self.alert_id = str(uuid.uuid4())
        self.resource_id = resource.resource_id
        self.title = title
        self.message = message
