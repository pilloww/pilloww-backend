import os
import sys
from sqlalchemy import desc
from fastapi import FastAPI, HTTPException, status, Security, Depends
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security.api_key import APIKeyHeader, APIKey

from app.analysis.disease_model import DiseaseModel, UtilizationModelParams, DiseaseModelReport
from app.model.base import Base, engine, Session
from app.model.entities import *

from app.views.hospitals import HospitalsResponse, HospitalView, AddressView, GeolocationView, ResourceView, AlertView
from app.views.resource_updates import ResourceUpdateParams, ResourceParams
from app.views.resources import ResourceDetailResponse, ResourceDetailView, ResourceTransactionView
from app.views.utilization_model import UtilizationModelParams, UtilizationModelReport

app = FastAPI(title="pilloww", description="This API is just a demo", version="0.3.14-preAlpha")
Base.metadata.create_all(engine)

#TODO: Change this, it is just a demo
API_KEY = "Ca3jair2ao5aingaigha"
API_KEY_NAME = "Authorization"
api_key_header = APIKeyHeader(name=API_KEY_NAME, auto_error=False)

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def get_api_key(api_key_header: str = Security(api_key_header)):
    if api_key_header == API_KEY:
        return api_key_header
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Could not validate credentials"
        )


@app.get("/hospitals", response_model=HospitalsResponse)
def read_hospitals(offset: int = None, limit: int = None) -> HospitalsResponse:
    session = Session()

    query = session.query(Hospital)
    if offset is not None:
        query = query.offset(offset)
    if limit is not None:
        query = query.limit(limit)

    hospitals: List[Hospital] = query.all()

    return HospitalsResponse(
        hospitals=[
            HospitalView(
                id=hospital.hospital_id,
                name=hospital.label,
                address=AddressView(
                    postcode=hospital.address.postal_code,
                    address_line1=hospital.address.address_line1,
                    address_line2=hospital.address.address_line2,
                    locality=hospital.address.locality,
                ) if hospital.address is not None else None,
                geolocation=GeolocationView(
                    latitude=hospital.coordinates.latitude,
                    longitude=hospital.coordinates.longitude,
                ) if hospital.coordinates is not None else None,
                resources=[
                    ResourceView(
                        id=resource.resource_id,
                        label=resource.label,
                        type=resource.resource_type,
                        available_amount=max(resource.availabilityChanges, key=lambda c: c.timestamp.timestamp()).count if len(resource.availabilityChanges) > 0 else 0,
                        available_amount_timestamp=int(max([c.timestamp.timestamp() for c in resource.availabilityChanges])) if len(resource.availabilityChanges) > 0 else 0,
                        utilized_amount=max(resource.utilizationChanges, key=lambda c: c.timestamp.timestamp()).count if len(resource.utilizationChanges) > 0 else 0,
                        utilized_amount_timestamp=int(max([c.timestamp.timestamp() for c in resource.utilizationChanges])) if len(resource.utilizationChanges) > 0 else 0,
                        alerts=[AlertView(id=alert.alert_id, title=alert.title, message=alert.message) for alert in resource.alerts]
                    )
                    for resource in hospital.resources
                ]
            )
            for hospital in hospitals
        ]
    )


@app.post("/resource/availability/{resource_id}", status_code=status.HTTP_201_CREATED)
def post_resource(resource_id: str, update: ResourceUpdateParams):
    session = Session()

    resource: Optional[Resource] = session.query(Resource).filter_by(resource_id=resource_id).first()

    if resource is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource not found")
    transaction = ResourceAvailabilityTransaction(resource=resource, count=update.count)
    session.add(transaction)

    session.commit()

    return {}


@app.post("/resource/utilization/{resource_id}", status_code=status.HTTP_201_CREATED)
def post_resource(resource_id: str, update: ResourceUpdateParams):
    session = Session()

    resource: Optional[Resource] = session.query(Resource).filter_by(resource_id=resource_id).first()

    if resource is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource not found")
    transaction = ResourceUtilizationTransaction(resource=resource, count=update.count)
    session.add(transaction)

    session.commit()

    return {}


@app.post('/resource/add', status_code=status.HTTP_201_CREATED, response_model=ResourceDetailView)
def add_resource(resource_params: ResourceParams):
    session = Session()

    hospital: Optional[Hospital] = session.query(Hospital).filter_by(hospital_id=resource_params.hospital_id).first()
    if hospital is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Hospital with id '{resource_params.hospital_id}' not found.")

    # if resource_params.type not in {'normalCare', 'intermediateCare', 'intensiveCare'}:
    #     raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Resource type '{resource_params.type}' is invalid.")

    resource = Resource(
        label=resource_params.label,
        type=resource_params.type,
        hospital=hospital,
        description=None
    )
    session.add(resource)
    session.commit()

    return ResourceDetailView(
        id=resource.resource_id,
        type=resource.resource_type,
        label=resource.label,
        availability=[],
        utilization=[],
        alerts=[]
    )


@app.get('/resource/{resource_id}', response_model=ResourceDetailResponse)
async def get_resource(resource_id: str):

    session = Session()

    resource: Optional[Resource] = session.query(Resource).filter_by(resource_id=resource_id).first()
    if resource is None:
        raise HTTPException(status_code=404, detail=f"Resource with id '{resource_id}' not found.")

    return ResourceDetailResponse(
        resources=[
            ResourceDetailView(
                id=resource.resource_id,
                type=resource.resource_type,
                label=resource.label,
                availability=[
                    ResourceTransactionView(
                        id=transaction.transaction_id,
                        timestamp=int(transaction.timestamp.timestamp()),
                        value=transaction.count
                    )
                    for transaction in resource.availabilityChanges
                ],
                utilization=[
                    ResourceTransactionView(
                        id=transaction.transaction_id,
                        timestamp=int(transaction.timestamp.timestamp()),
                        value=transaction.count
                    )
                    for transaction in resource.utilizationChanges
                ],
                alerts=[]
            )
        ]
    )


@app.post('/hospital/model_utilization/{hospital_id}', response_model=UtilizationModelReport)
def model_utilization(hospital_id: str, params: UtilizationModelParams):
    session = Session()
    hospital: Optional[Hospital] = session.query(Hospital).filter_by(hospital_id=hospital_id).first()
    if hospital is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Hospital with id '{hospital_id}' not found.")

    model = DiseaseModel(hospital, params)
    report = model.fit()

    return UtilizationModelReport(
        critical=report.critical,
        peak_time=report.peak_time,
        peak_utilizations=report.peak_utilizations,
        samples=[
            [t, y] for t, y in report.samples
        ]
    )
