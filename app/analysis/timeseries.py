import numpy as np
from abc import ABC, abstractmethod
from typing import List, Tuple, Optional


class TimeSeriesPredictor(ABC):
    def __init__(self):
        self.points: List[Tuple[float, float]] = []


    def add_value(self, time: float, value: float):
        self.points.append((time, value))


    @abstractmethod
    def fit(self) -> 'TimeSeriesPredictor':
        return self


    @abstractmethod
    def predict(self, time: float):
        return 0


class ExponentialRegressionPredictor(TimeSeriesPredictor):
    def __init__(self):
        super().__init__()
        self.polynomial: Optional[np.ndarray] = None

    def fit(self) -> 'TimeSeriesPredictor':
        data = np.array(self.points)
        t = data[:, 0]
        y = data[:, 1]
        log_y = np.log(y)
        self.polynomial = np.polyfit(t, log_y, 1, w=t / (np.max(t) - np.min(t)))


    def growth_rate(self) -> float:
        if self.polynomial is None:
            self.fit()
        return self.polynomial[1]


    def predict(self, time: float):
        if self.polynomial is None:
            self.fit()

        y = np.sum(np.exp(np.array([1, time]) * self.polynomial))
        return y

