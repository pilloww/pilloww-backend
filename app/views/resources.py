from typing import List

from pydantic import BaseModel


class ResourceTransactionView(BaseModel):
    id: str
    timestamp: int
    value: int


class ResourceAlertView(BaseModel):
    id: str
    title: str
    message: str


class ResourceDetailView(BaseModel):
    id: str
    type: str
    label: str
    availability: List[ResourceTransactionView]
    utilization: List[ResourceTransactionView]
    alerts: List[ResourceAlertView]


class ResourceDetailResponse(BaseModel):
    resources: List[ResourceDetailView]
