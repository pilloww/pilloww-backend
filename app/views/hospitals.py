from typing import List, Optional

from pydantic import BaseModel


class AddressView(BaseModel):
    address_line1: str
    address_line2: Optional[str]
    postcode: str
    locality: str


class GeolocationView(BaseModel):
    latitude: float
    longitude: float


class AlertView(BaseModel):
    id: str
    title: str
    message: str


class ResourceView(BaseModel):
    id: str
    type: str
    label: str
    available_amount: int
    available_amount_timestamp: int
    utilized_amount: int
    utilized_amount_timestamp: int
    alerts: List[AlertView]


class HospitalView(BaseModel):
    id: str
    name: str
    address: Optional[AddressView]
    geolocation: Optional[GeolocationView]
    resources: List[ResourceView]


class HospitalsResponse(BaseModel):
    hospitals: List[HospitalView]