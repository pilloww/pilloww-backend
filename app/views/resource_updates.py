from pydantic import BaseModel


class ResourceUpdateParams(BaseModel):
    count: int


class ResourceParams(BaseModel):
    type: str
    label: str
    hospital_id: str
