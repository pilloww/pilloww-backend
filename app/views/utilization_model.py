from typing import List, Dict

from pydantic import BaseModel


class UtilizationModelParams(BaseModel):
    growth_rate: float
    surrounding_population: float
    expected_infected_fraction: float
    expected_hospitalization_fraction: float
    expected_hospitalization_period: float


class UtilizationModelReport(BaseModel):
    critical: bool
    peak_utilizations: Dict[str, float]
    peak_time: float
    samples: List[List[float]]
