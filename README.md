# pilloww-backend

## Run backend
### Quick Start docker
```bash
docker build . -t pilloww-backend
docker run -d -e ENVIRONMENT=DEVELOPMENT -p 80:80 --name pilloww-backend pilloww-backend:latest
```

### Quick Start local
```bash
git clone https://gitlab.com/pilloww/pilloww-backend.git
cd pilloww-backend
```

1. Clone the repo
```bash
git clone https://gitlab.com/pilloww/pilloww-backend.git
cd pilloww-backend
```

2. Initialize and activate a virtualenv:
```bash
python -m venv venv
source venv/bin/activate
```

3. Install the dependencies:
```bash
pip install -r requirements.txt
```

5. Run the development server:
```bash
export PYTHONUNBUFFERED=1
export ENVIRONMENT=DEVELOPMENT
unicorn app.main:app --reload 
```


## Import Hospitals

Hospitals can be collected from OpenStreetMap via the Overpass API (e.g. on [overpass-turbo.eu](https://overpass-turbo.eu/)) with the following query:

```
[out:json];
{{geocodeArea:Deutschland}}->.searchArea;
(
  node[amenity=hospital](area.searchArea);
  relation[amenity=hospital](area.searchArea);
  way[amenity=hospital](area.searchArea);
);
out center;
```

### Docker
```bash
docker exec pilloww-backend /bin/bash
python3 importer.py 
```

### local
```bash
export ENVIRONMENT=DEVELOPMENT
python3 importer.py
```

